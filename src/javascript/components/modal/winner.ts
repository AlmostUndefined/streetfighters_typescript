import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import { FighterInterface } from '../../interfaces/fighter.interface'

export function showWinnerModal(fighter: FighterInterface): void {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `${fighter.name} win`,
    bodyElement: imageElement,
    onClose
  };
  
  showModal(modalElement);  
}

function onClose(): void {
  window.location.reload()
};
