import { createElement } from '../helpers/domHelper';
import { FighterInterface } from '../interfaces/fighter.interface';

export function createFighterPreview(fighter: FighterInterface, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const {name, health, attack, defense} = fighter;
    
    fighterElement.innerHTML += `Name: ${name}<br> Health: ${health}<br> Attack: ${attack}<br> Defense: ${defense}<br>`;
    fighterElement.style.color = '#F1C40F'
  };

  return fighterElement;
}

export function createFighterImage(fighter: FighterInterface): HTMLElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
