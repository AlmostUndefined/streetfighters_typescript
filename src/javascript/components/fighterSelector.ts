import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
// @ts-ignore
import versusImg  from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterInterface } from '../interfaces/fighter.interface';

export function createFightersSelector() {
  let selectedFighters: Array<FighterInterface> = [];

  return async (event: Event, fighterId: string): Promise<void> => {
    const fighter: FighterInterface = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ? playerOne : fighter;
    const secondFighter = Boolean(playerOne) ? (playerTwo ? playerTwo : fighter) : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, FighterInterface> = new Map();

export async function getFighterInfo(fighterId: string): Promise<FighterInterface> {
  try {
    const fighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
    return fighterDetails;
  } catch (error) {
    throw error;
  }
}

function renderSelectedFighters(selectedFighters: Array<FighterInterface>): void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<FighterInterface>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<FighterInterface>): void {
  renderArena(selectedFighters);
}
