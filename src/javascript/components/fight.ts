import { controls } from '../../constants/controls';
import { FighterInterface } from '../interfaces/fighter.interface';

export async function fight(firstFighter: FighterInterface, secondFighter: FighterInterface): Promise<FighterInterface> {
  return new Promise((resolve) => {
    const selectedFirstFighter = {
      ...firstFighter,
      currentHealth: firstFighter.health,
      block: false,
      refresh: new Date(Date.now()-10000)
    }
    const selectedSecondFighter = {
      ...secondFighter,
      currentHealth: secondFighter.health,
      block: false,
      refresh: new Date(Date.now()-10000)
    }
    
    let pressed: Set<string> = new Set();

    document.addEventListener('keydown', (key) => {
      pressed.add(key.code);

      if(firstFighterBlock(key)) {
        selectedFirstFighter.block = true;
      }
      if(secondFighterBlock(key)) {
        selectedSecondFighter.block = true;
      }

      criticalAttack(selectedFirstFighter, selectedSecondFighter, pressed);

      const winner = endOfTheBattle(selectedFirstFighter, selectedSecondFighter) 
      if (winner) {
        resolve(winner);
      };
    });
    
    document.addEventListener('keyup', (key) => {
      pressed.delete(key.code);
      
      if(firstFighterOutOfBlock(key)) {
        selectedFirstFighter.block = false;
      }
      if(secondFighterOutOfBlock(key)) {
        selectedSecondFighter.block = false;
      }

      simpleAttack(selectedFirstFighter, selectedSecondFighter, key);

      const winner = endOfTheBattle(selectedFirstFighter, selectedSecondFighter) 
      if (winner) {
        resolve(winner);
      };
    });
  });
}


function firstFighterBlock(key: KeyboardEvent): boolean {
  if (key.code === controls.PlayerOneBlock) {
    return true;
  }
}
function secondFighterBlock(key: KeyboardEvent): boolean {
  if (key.code === controls.PlayerTwoBlock) {
    return true;
  }
}
function firstFighterOutOfBlock(key: KeyboardEvent): boolean {
  if (key.code === controls.PlayerOneBlock) {
    return true;
  }
}
function secondFighterOutOfBlock(key: KeyboardEvent): boolean {
  if (key.code === controls.PlayerTwoBlock) {
    return true;
  }
}

function endOfTheBattle (firstFighter: FighterInterface, secondFighter: FighterInterface): FighterInterface {
  if (firstFighter.currentHealth <= 0 || secondFighter.currentHealth <= 0) {
    const winner = firstFighter.currentHealth <= 0 ? secondFighter : firstFighter;
    return winner;
  }
  return null;
}

function criticalAttack(firstFighter: FighterInterface, secondFighter: FighterInterface, keys: Set<string>): void {
  const firstHealthIndicator = document.getElementById('left-fighter-indicator');
  const secondHealthIndicator = document.getElementById('right-fighter-indicator');
  if (controls.PlayerOneCriticalHitCombination.every(key => keys.has(key))) {
    criticalAttackHit(firstFighter, secondFighter, secondHealthIndicator);
  }
  if (controls.PlayerTwoCriticalHitCombination.every(key => keys.has(key))) {
    criticalAttackHit(secondFighter, firstFighter, firstHealthIndicator);
  }
}


function simpleAttack(firstFighter: FighterInterface, secondFighter: FighterInterface, key: KeyboardEvent): void {
  const firstHealthIndicator = document.getElementById('left-fighter-indicator');
  const secondHealthIndicator = document.getElementById('right-fighter-indicator');

  if (key.code == controls.PlayerOneAttack) {
    simpleAttackHit(firstFighter, secondFighter, secondHealthIndicator);
  }
  if (key.code == controls.PlayerTwoAttack) {
    simpleAttackHit(secondFighter, firstFighter, firstHealthIndicator);
  }
}

function criticalAttackHit(attacker: FighterInterface, defender: FighterInterface, healthIndicator: HTMLElement): void {
  if (attacker.block) {
    return;
  }   

  if ((new Date().getTime() - attacker.refresh.getTime()) > 10000 ) {
    defender.currentHealth -= attacker.attack * 2;

    const { health, currentHealth } = defender;
    const indicatorWidth = currentHealth * 100 / health;
    healthIndicator.style.width = `${indicatorWidth}%`;

    attacker.refresh = new Date();
  }
}

function simpleAttackHit(attacker: FighterInterface, defender: FighterInterface, healthIndicator: HTMLElement): void {
  if (attacker.block || defender.block) {
    return;
  }  

  defender.currentHealth -= getDamage(attacker, defender);

  const { health, currentHealth } = defender;
  const indicatorWidth = currentHealth * 100 / health;
  healthIndicator.style.width = `${indicatorWidth}%`;
}

export function getDamage(attacker: FighterInterface, defender: FighterInterface): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) {
    return damage
  }
  return 0;
}

export function getHitPower(fighter: FighterInterface): number {
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterInterface): number {
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance; 
}
