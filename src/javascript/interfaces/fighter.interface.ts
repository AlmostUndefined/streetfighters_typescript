export interface FighterInterface {
  attack: number;
  block?: boolean;
  currentHealth?: number;
  defense: number;
  health: number;
  name: string;
  refresh?: Date;
  source: string;
  _id: string;
}