export interface ModalInterface {
  title: string,
  bodyElement: HTMLElement,
  onClose: ()=> void
}