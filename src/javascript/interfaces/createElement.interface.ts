export interface CreateElementInterface { 
  tagName: string, 
  className?: string, 
  attributes?: Attributes
}

export interface Attributes {
  [index: string]: string;
}